var express = require('express');
var router = express.Router();

var passport = require('passport');
var auth = require('../middlewares/auth');
router.get('/login', function(req, res) {
  res.render('login');
});

router.post('/login',
    passport.authenticate('local', {
      successRedirect: '/loginSuccess',
      failureRedirect: '/loginFailure'
    })
);

router.get('/loginFailure', function(req, res, next) {
  res.send('Failed to authenticate');
});

router.get('/loginSuccess',auth.isAuthenticated, function(req, res, next) {
  //if(!req.isAuthenticated()){
  //    res.redirect('/login');
  //}
  res.send('Successfully authenticated'+req.user.id+req.user.username);
});



router.get('/profile', function(req, res, next){
  res.send('Successfully profile');
});

module.exports = router;
