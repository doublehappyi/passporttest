/**
 * Created by db on 16/1/10.
 */

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
passport.serializeUser(function(user, done) {
    console.log("serializeUser: ", user);
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    console.log("deserializeUser: ", user);
    done(null, user);
});

var mysql = require('mysql');
var db_conf = {
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '11111111',
    database: 'sopr'
}
var pool = mysql.createPool(db_conf);
var localStrategy = new LocalStrategy(function(username, password, done) {
    process.nextTick(function() {
        pool.getConnection(function (err, conn) {
            if (err) {
                return;
            }
            var sql_str = 'select id, username from sopr_user where username=? and password=?';
            sql_str = mysql.format(sql_str, [username, password]);
            conn.query(sql_str, function (err, rows, fields) {
                if (err) {
                    return;
                }
                console.log('rows[0]', rows[0]);
                done(null, rows[0]);
                conn.release();
            });
        });
    });
})

module.exports = {
    isAuthenticated:function(req, res, next){
        if(req.isAuthenticated()){
            next()
        }else{
            res.redirect('/login');
        }
    },
    localStrategy:localStrategy
}